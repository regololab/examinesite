# README #


### EXAMINESITE v0.1b ###

1. Inserire le url dei prodotti del sito aliexpress all'interno del file url.csv separati dall'accapo (new line \n)
2. Assicurarsi che il DB in sqlite (site.db) e la directory ./css/ abbiano i permessi in scrittura e/o esecuzione (su linux si potrà utilizzare il file modifica_permessi.sh)
3. Assicurarsi di avere installato python 2.7.x (in python 3.x potrebbe avere problemi)
4. Avviare lo script tramite comando da console: python start.py
5. Attendere la conclusione dello script e verificare i dati all'interno del DB
6. estrapolare i dati dal DB site.db presente nella directory del programma.

**Importante:**

- Lo script è stato testato unicamente in ambiente Linux Mint 18, in generale non dovrebbero esserci problemi con altri sistemi operativi compreso Windows ®
- Lo script è stato testato per un numero limitato di pagine
- Eventuali danni causati dallo script non sono in alcun modo imputabili al suo realizzatore.
- Software distribuito sotto licenza [CC BY-SA ](https://creativecommons.org/licenses/by-sa/3.0/)
[![CC-BY-SA_icon.svg.png](https://bitbucket.org/repo/x4BRq8/images/4282204093-CC-BY-SA_icon.svg.png)](https://creativecommons.org/licenses/by-sa/3.0/)