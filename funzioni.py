#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
ExamineSite 0.1b (https://bitbucket.org/regololab/examinesite)
Copyright 2016 leonardo lo titolo
Licensed under:
Attribuzione - Condividi allo stesso modo CC BY-SA
https://creativecommons.org/licenses/by-sa/3.0/it/
https://creativecommons.org/licenses/by-sa/3.0/
https://creativecommons.org/licenses/by-sa/3.0/legalcode
'''

import urllib2
import sqlite3
import re
import urllib
import shutil
from os import listdir, path, remove, unlink
from os.path import isfile, join
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
__this_path = path.dirname(path.realpath(__file__))
conn = sqlite3.connect(__this_path+'/site.db')

def printProgress (iteration, total, prefix = '', suffix = '', decimals = 1, barLength = 100):
    formatStr = "{0:." + str(decimals) + "f}"
    percent = formatStr.format(100 * (iteration / float(total)))
    filledLength = int(round(barLength * iteration / float(total)))
    bar = '█' * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percent, '%', suffix)),
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()

def __save_color(style,classe):
    for R in style:
        if R.find(classe) >= 0:
            return R

def data_sheet(k, v):
    desc = []
    n = 0
    for R in k:
        if R:
            desc.append({
                R.string: v[n].string.rstrip('\r\n')
            })
            n = n + 1
    return desc

def estrai_dati_colore(v, classe):
    stringa = ''
    for r in v:
        stringa = str(r.get('href', ''))
        if stringa.find('.css') >= 0:
            stringa = stringa.replace("http://","").replace("https://","").replace("://","").replace("//","")
            urllib.urlretrieve ('https://'+stringa, './css/'+stringa.split('/')[-1])

    onlyfiles = [f for f in listdir('./css/') if isfile(join('./css/', f))]
    stri = []
    colore = ''
    for R in onlyfiles:
        f = open('./css/'+R, 'r')
        stri = f.read().split('.')
        f.close()
        colore = __save_color(stri,classe)
        try:
            remove(__this_path + '/css/'+R)
        except:
            pass
        if colore is not None:
            return colore
        else:
            pass

def delete_db(table):
    c = conn.cursor()
    c.execute('''DELETE FROM %s;''' % (table))
    conn.commit()

    c = conn.cursor()
    c.execute('''DELETE FROM sqlite_sequence WHERE name = '%s';''' % (table))
    conn.commit()

def read_url():
    f = open(__this_path+'/url.csv', 'r')
    url = f.read()
    return url.splitlines()

def insert_2db(diz):
    # GENERALI .................................................................
    c = conn.cursor()
    c.execute('''INSERT INTO generali(
            breadcrumbs,
            titolo,
            moneta_corrente,
            prezzo_intero,
            prezzo_scontato,
            percentuale_sconto,
            reviewCount,
            ratingValue
        ) values(
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
        ) ;''' % (
            diz.get('breadcrumbs').replace("'","''"),
            diz.get('titolo_prodotto').replace("'","''"),
            diz.get('moneta_corrente').replace("'","''"),
            str(diz.get('prezzo_intero')).replace("'","''"),
            str(diz.get('prezzo_scontato')).replace("'","''"),
            str(diz.get('percentuale_sconto')).replace("'","''"),
            str(diz.get('reviewCount')).replace("'","''"),
            str(diz.get('ratingValue')).replace("'","''"),
        ))
    conn.commit()
    last_id = c.lastrowid

    # FORMATI .................................................................
    for R in diz.get('formati'):
        cur1 = conn.cursor()
        cur1.execute('''INSERT INTO formati(
                id_generali,
                formati
            ) values(
                '%s',
                '%s'
            ) ;''' % (
                last_id,
                R.replace("'","''"),
            ))
        conn.commit()

    # COLORI .................................................................
    for Rfor in diz.get('colori'):
        cur2 = conn.cursor()
        cur2.execute('''INSERT INTO colori(
                id_generali,
                colore_class,
                title
            ) values(
                '%s',
                '%s',
                '%s'
            ) ;''' % (
                last_id,
                Rfor.get('colore_class').replace("'","''"),
                Rfor.get('title').replace("'","''")
            ))
        conn.commit()

    # IMG COLORI ...............................................................
    for Rimgc in diz.get('img_color'):
        cur2 = conn.cursor()
        cur2.execute('''INSERT INTO colori(
                id_generali,
                title,
                img50x50,
                img
            ) values(
                '%s',
                '%s',
                '%s',
                '%s'
            ) ;''' % (
                last_id,
                Rimgc.get('title').replace("'","''"),
                Rimgc.get('image_50x50').replace("'","''"),
                Rimgc.get('image').replace("'","''"),
            ))
        conn.commit()

    # IMMAGINI .................................................................
    for Rimg in diz.get('immagini'):
        cur3 = conn.cursor()
        cur3.execute('''INSERT INTO immagini(
                id_generali,
                alt,
                image,
                image_50x50,
                ordine
            ) values(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            ) ;''' % (
                last_id,
                Rimg.get('alt').replace("'","''"),
                Rimg.get('image').replace("'","''"),
                Rimg.get('image_50x50').replace("'","''"),
                Rimg.get('order'),
            ))
        conn.commit()

    # SCHEDA TECNICA............................................................
    for Rst in diz.get('scheda_tecnica'):
        my_list = Rst.items()[0]
        #print my_list[0] + my_list[1]
        cur4 = conn.cursor()
        cur4.execute('''INSERT INTO scheda_tecnica(
                id_generali,
                key,
                value
            ) values(
                '%s',
                '%s',
                '%s'
            ) ;''' % (
                last_id,
                my_list[0].replace("'","''"),
                my_list[1].replace("'","''")
            ))
        conn.commit()
