#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
ExamineSite 0.1b (https://bitbucket.org/regololab/examinesite)
Copyright 2016 leonardo lo titolo
Licensed under:
Attribuzione - Condividi allo stesso modo CC BY-SA
https://creativecommons.org/licenses/by-sa/3.0/it/
https://creativecommons.org/licenses/by-sa/3.0/
https://creativecommons.org/licenses/by-sa/3.0/legalcode
'''

import urllib2
import json
import sys
import shutil
from lxml.etree import fromstring
from bs4 import BeautifulSoup
from funzioni import data_sheet, estrai_dati_colore, insert_2db, delete_db, read_url, printProgress
from time import sleep

def import_dati(url2):
    content_site = {}
    htmlf = urllib2.urlopen(url2)
    soup = BeautifulSoup(htmlf, 'html.parser')
    # estrai css ............................
    css = soup.find_all("link")
    # titolo prodotto ............................
    title_h1 = soup.find_all("h1", { "class" : "product-name" })
    _titolo_prodotto = title_h1[0].string.rstrip('\r\n') if title_h1 is not None else ''
    content_site.update({"titolo_prodotto":_titolo_prodotto})
    # breadcrumbs ............................
    breadcrumbs = soup.h2.string
    _breadcrumbs = breadcrumbs.rstrip('\r\n') if breadcrumbs is not None else ''
    content_site.update({"breadcrumbs":_breadcrumbs})
    # voti ............................
    voti_star = soup.find_all("span", { "itemprop" : "ratingValue" })
    _voti_star = voti_star[0].string.rstrip('\r\n') if voti_star is not None else ''
    content_site.update({"ratingValue":_voti_star})
    reviewCount = soup.find_all("span", { "itemprop" : "reviewCount" })
    _reviewCount = reviewCount[0].string.rstrip('\r\n') if reviewCount is not None else ''
    content_site.update({"reviewCount":_reviewCount})
    # prezzo intero ............................
    prezzo_intero = soup.find(id="j-sku-price")
    _prezzo_intero = prezzo_intero.string.rstrip('\r\n') if prezzo_intero is not None else ''
    content_site.update({"prezzo_intero":_prezzo_intero})
    # prezzo scontato ............................
    prezzo_scontato = soup.find(id="j-sku-discount-price")
    _prezzo_scontato = prezzo_scontato.string.rstrip('\r\n') if prezzo_scontato is not None else ''
    content_site.update({"prezzo_scontato":_prezzo_scontato})
    # moneta corrente ............................
    moneta_corrente = soup.find_all("span", { "class" : "p-symbol", "itemprop":"priceCurrency" })
    _moneta_corrente = moneta_corrente[0].string.rstrip('\r\n') if moneta_corrente is not None else ''
    content_site.update({"moneta_corrente":_moneta_corrente})
    # percentuale scontato ............................
    percent_sale = soup.find_all("span", { "class" : "p-discount-rate"})
    if percent_sale is not None:
        if len(percent_sale) > 0:
            _percent_sale = percent_sale[0].string.rstrip('\r\n')
        else:
            _percent_sale = ''
    else:
        _percent_sale = ''
    content_site.update({"percentuale_sconto":_percent_sale})
    #immagini miniature ............................
    immagini = []
    n = 0
    for thumb_img in soup.find_all("span", { "class" : "img-thumb-item" }):
        immagini.append({
            "order": n,
            "alt": thumb_img.find_all("img")[0].get('alt', '').rstrip('\r\n'),
            "image_50x50": thumb_img.find_all("img")[0].get('src', '').rstrip('\r\n'),
            "image": thumb_img.find_all("img")[0].get('src', '').replace(".jpg_50x50", "").replace('\n', ''),
        })
        n = n + 1
    content_site.update({"immagini":immagini})
    # colori ............................
    colors = []
    for clrs in soup.find_all("li", { "class" : "item-sku-color"}):
        element = clrs.find_all('span')[0].get('class')[0]
        colors.append({
            'title': clrs.find_all('span')[0].get('title'),
            'colore_class': '.'+estrai_dati_colore(css, element)
        })
    content_site.update({"colori":colors})
    # colori immagini............................
    img_color = []
    for clrs in soup.find_all("ul", { "id" : "j-sku-list-1"}):
        element = clrs.find_all('img')
        for imc in element:
            img_color.append({
                "title": imc.get('title', '').rstrip('\r\n'),
                "image_50x50": imc.get('src', '').rstrip('\r\n'),
                "image": imc.get('bigpic', '').rstrip('\r\n'),
            })
    content_site.update({"img_color":img_color})
    # formato ............................
    formato = []
    for clrs in soup.find_all("ul", { "id" : "j-sku-list-2"}):
        element = clrs.find_all('span')
        for e in element:
            formato.append(e.string)
    content_site.update({"formati":formato})
    # scheda_tecnica ............................
    data_sheet_key = soup.find_all("span", { "class" : "propery-title"})
    data_sheet_val = soup.find_all("span", { "class" : "propery-des"})
    content_site.update({"scheda_tecnica":data_sheet(data_sheet_key, data_sheet_val)})

    insert_2db(content_site)

if __name__ == "__main__":
    url = read_url()
    delete_db('colori')
    delete_db('formati')
    delete_db('generali')
    delete_db('immagini')
    delete_db('scheda_tecnica')
    # progress bar
    items = list(range(0, len(url)))
    i = 0
    l = len(items)
    printProgress(i, l, prefix = 'Progress:', suffix = 'Complete', barLength = 50)

    for Rurl in url:
        i += 1
        import_dati(Rurl)
        printProgress(i, l, prefix = 'Progress:', suffix = 'Complete', barLength = 50)
